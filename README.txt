CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module gives the possibility to transform an image whatever the type into 
an image in the form webp

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/image_webp_toolkit

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/image_webp_toolkit


REQUIREMENTS
------------

 * To use this module you must install the Lib GD with Webp


INSTALLATION
------------

 * Use `composer require drupal/image_webp_toolkit` to install.


CONFIGURATION
-------------

/admin/config/media/image-styles


MAINTAINERS
-----------

Current maintainers:
 * sofiene chaari (sofiene.chaari) - https://www.drupal.org/u/sofienechaari