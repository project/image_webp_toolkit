<?php


namespace Drupal\image_webp_toolkit;


use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\image_webp_toolkit\ImageToolkit\ImageToolkitManager;

/**
 * Class ImageWebpToolkitServiceProvider
 *
 * @package Drupal\image_webp_toolkit
 */
class ImageWebpToolkitServiceProvider extends ServiceProviderBase {

  /**
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('image.toolkit.manager')
      ->setClass(ImageToolkitManager::class);
  }

}
