<?php

namespace Drupal\image_webp_toolkit\ImageToolkit;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\ImageToolkit\ImageToolkitManager as BaseImageToolkitManager;

class ImageToolkitManager extends BaseImageToolkitManager {

  /**
   * ImageToolkitManager constructor.
   *
   * @param \Traversable $namespaces
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(\Traversable           $namespaces, CacheBackendInterface $cache_backend,
                              ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct($namespaces, $cache_backend, $module_handler, $config_factory);
    $this->alterInfo('image_toolkit_plugins');
    $this->setCacheBackend($cache_backend, 'image_toolkit_plugins');
  }

}
