<?php


namespace Drupal\image_webp_toolkit;

use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\system\Plugin\ImageToolkit\GDToolkit as BaseGDToolkit;

class GDToolkit extends BaseGDToolkit {

  /**
   * @return array
   */
  protected static function supportedTypes() {
    $gdInfo = gd_info();
    $supportedTypes = parent::supportedTypes();
    if (!empty($gdInfo['WebP Support'])) {
      $supportedTypes[] = IMAGETYPE_WEBP;
    }
    return $supportedTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function save($destination) {
    $scheme = StreamWrapperManager::getScheme($destination);
    // Work around lack of stream wrapper support in imagejpeg() and imagepng().
    if ($scheme && $this->streamWrapperManager->isValidScheme($scheme)) {
      // If destination is not local, save image to temporary local file.
      $local_wrappers = $this->streamWrapperManager->getWrappers(StreamWrapperInterface::LOCAL);
      if (!isset($local_wrappers[$scheme])) {
        $permanent_destination = $destination;
        $destination = $this->fileSystem->tempnam('temporary://', 'gd_');
      }
      // Convert stream wrapper URI to normal path.
      $destination = $this->fileSystem->realpath($destination);
    }

    $function = 'image' . image_type_to_extension($this->getType(), FALSE);
    if (!function_exists($function)) {
      return FALSE;
    }
    if ($this->getType() == IMAGETYPE_JPEG) {
      $success = $function($this->getResource(), $destination, $this->configFactory->get('system.image.gd')
        ->get('jpeg_quality'));
    }
    else {
      // Always save PNG images with full transparency.
      if ($this->getType() == IMAGETYPE_PNG) {
        imagealphablending($this->getResource(), TRUE);
        imagesavealpha($this->getResource(), TRUE);
      }
      $success = $function($this->getResource(), $destination);
    }
    // Move temporary local file to remote destination.
    if (isset($permanent_destination) && $success) {
      try {
        $this->fileSystem->move($destination, $permanent_destination, FileSystemInterface::EXISTS_REPLACE);
        return TRUE;
      } catch (FileException $e) {
        return FALSE;
      }
    }
    return $success;
  }

}
