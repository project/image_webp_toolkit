<?php


namespace Drupal\image_webp_toolkit;

use Drupal\Component\Utility\Color;
use Drupal\system\Plugin\ImageToolkit\Operation\gd\CreateNew as BaseCreateNew;

class CreateNew extends BaseCreateNew {

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments) {
    // Get the image type.
    $type = $this->getToolkit()->extensionToImageType($arguments['extension']);

    // Store the original GD resource.
    $original_res = $this->getToolkit()->getResource();

    // Create the resource.
    if (!$res = imagecreatetruecolor($arguments['width'], $arguments['height'])) {
      return FALSE;
    }

    // Fill the resource with transparency as possible.
    switch ($type) {
      case IMAGETYPE_PNG:
        imagealphablending($res, FALSE);
        $transparency = imagecolorallocatealpha($res, 0, 0, 0, 127);
        imagefill($res, 0, 0, $transparency);
        imagealphablending($res, TRUE);
        imagesavealpha($res, TRUE);
        break;
      case IMAGETYPE_GIF:
        if (empty($arguments['transparent_color'])) {
          $fill_color = imagecolorallocatealpha($res, 255, 255, 255, 127);
        }
        else {
          $fill_rgb = Color::hexToRgb($arguments['transparent_color']);
          $fill_color = imagecolorallocatealpha($res, $fill_rgb['red'], $fill_rgb['green'], $fill_rgb['blue'], 127);
          imagecolortransparent($res, $fill_color);
        }
        imagefill($res, 0, 0, $fill_color);
        break;
      case IMAGETYPE_JPEG:
      case IMAGETYPE_WEBP:
        imagefill($res, 0, 0, imagecolorallocate($res, 255, 255, 255));
        break;
      default:
        break;

    }

    // Update the toolkit properties.
    $this->getToolkit()->setType($type);
    $this->getToolkit()->setResource($res);

    // Destroy the original resource if it is not needed by other operations.
    if (!$arguments['is_temp'] && is_resource($original_res)) {
      imagedestroy($original_res);
    }

    return TRUE;
  }

}
